﻿/*
Author: Agustín Kohan
Date: 19/09/2022
*/

using GameTools;
using MyFirstProgram;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace dotnetcore
{
    class MatrixGame : GameEngine
    {
        
        private int[,] matrixInfo = new int[25, 100];
        private MatrixRepresentation matrix = new MatrixRepresentation(25, 100);
        private static int _cursorPosition = 0;
        private int width; int height;
        private bool _randomMode = false;
        private bool _fullManualMode = false;

        public override void Start()
        {
            //Execution before starting the loop
            MenuObject menu = new MenuObject(new string[] {"Modo Automatico", "Interactivo(Con el teclado)", "Interactivo(Full manual)"});
            DisableConsoleResize.SetDisableResize();
            Console.CursorVisible = false;

            System.Threading.Thread.Sleep(2000);

            //Generamos el menu
            menu.Use("Elige un modo");
           
           //Accedemos a una de las opciones del manu
           Console.Clear();
            switch (menu.posCursor)
            {
                case 0: //Automatico
                    _randomMode = true;
                    break;
                case 1: //Interactivo
                    break;
                case 2:
                    _fullManualMode = true;
                    break;
            }
            matrix.CleanTheMatrix();
            fillMatrixInfo();
            Console.SetWindowSize(matrixInfo.GetLength(1), matrixInfo.GetLength(0) + 5);
            Console.SetBufferSize(matrixInfo.GetLength(1), matrixInfo.GetLength(0) + 5);


            if (_fullManualMode || !_randomMode)
            {
                Console.CursorTop = matrix.TheMatrix.GetLength(0);
                Console.WriteLine("Pulsa [<>] para mover el cursor");
                Console.WriteLine("Puedes usar el teclado numerico [0-9]");
                if(_fullManualMode) Console.Write("Manten pulsado [enter] para generar info");
                System.Threading.Thread.Sleep(2000);
            }
        }

        public override void Update()
        {
            //Execution ontime secuence of every frame
            if (Console.KeyAvailable == true) UpdateCursor();
            MoveALineDown();
            Trial();
            SetFirstLineMatrix();
            PaintChar();
        }

        public override void Exit()
        {
            //Code after last frame
            matrix.CleanTheMatrix();
            Console.Clear();
        }

        int GenerateRandom(int minvalue, int maxValue)
        {
            Random rnd = new Random();
            return rnd.Next(minvalue, maxValue);
        }

        int GenerateRandomWithExclude(int minvalue, int maxValue, int[]valuesToExclude)
        {
            int rnd;
            int valueIsExcluded;
            do
            {
                rnd = GenerateRandom(minvalue, maxValue);
                valueIsExcluded = Array.IndexOf(valuesToExclude, rnd);
            } while (valueIsExcluded != -1);

            return rnd;
        }

        void SetFirstLineMatrix()
        {
            if (_randomMode)
            {
                int rnd;
                bool flag = false;
                do
                {
                    rnd = GenerateRandom(0, matrix.TheMatrix.GetLength(1));
                    if (matrixInfo[0, rnd] == 0)
                    {

                        matrix.TheMatrix[0, rnd] = Convert.ToChar(GenerateRandomWithExclude(48, 123, new int[] { 58, 59, 60, 61, 62, 63, 64, 91, 92, 93, 94, 95, 96 })); //Mejorable
                        flag = true;
                    }
                } while (flag == false);

                matrixInfo[0, rnd] = 4;
            }
            else if(!_randomMode && !_fullManualMode)
            {   
                    if (matrixInfo[0, _cursorPosition] == 0)
                        matrix.TheMatrix[0, _cursorPosition] = Convert.ToChar(GenerateRandomWithExclude(48, 123, new int[] { 58, 59, 60, 61, 62, 63, 64, 91, 92, 93, 94, 95, 96 }));   
                    
                matrixInfo[0, _cursorPosition] = 4;
            }
            else
            {
                if (Console.KeyAvailable == true && Console.ReadKey(true).Key == ConsoleKey.Enter)
                {
                    if (matrixInfo[0, _cursorPosition] == 0)
                        matrix.TheMatrix[0, _cursorPosition] = Convert.ToChar(GenerateRandomWithExclude(48, 123, new int[] { 58, 59, 60, 61, 62, 63, 64, 91, 92, 93, 94, 95, 96 }));

                    matrixInfo[0, _cursorPosition] = 4;
                }
            }
        }

        void Trial()
        {
            for (int i = 0; i < matrix.TheMatrix.GetLength(1); i++)
            {
                if (matrixInfo[0, i] != 0)
                {
                    
                    matrix.TheMatrix[0, i] = Convert.ToChar(GenerateRandomWithExclude(48, 123, new int[] { 58, 59, 60, 61, 62, 63, 64, 91, 92, 93, 94, 95, 96 }));
                }
            }
        }

        void MoveALineDown()
        {
            bool lastline = true;
            for (int x = matrix.TheMatrix.GetLength(0)-1; x >= 0; x--)
            {
                for (int y = matrix.TheMatrix.GetLength(1)-1; y >=  0 ; y--)
                {
                    //Only do if it isn't the last line
                    if (!lastline)
                    {
                        //Moving the characters of the Matrix
                        matrix.TheMatrix[x+1, y] = matrix.TheMatrix[x,y];
                        matrix.TheMatrix[x, y] = ' ';

                        //Moving the info of the MatrixInfo
                        matrixInfo[x + 1, y] = matrixInfo[x, y];
                        if (matrixInfo[x,y] != 0) matrixInfo[x, y] -= 1;
                    }
                }
                lastline = false;
            }
        }

        void PaintChar()
        {
            for (int y = 0; y < matrix.TheMatrix.GetLength(0); y++)
            {
                for (int x = 0; x < matrix.TheMatrix.GetLength(1); x++)
                {
                    if (matrixInfo[y, x] != 0)
                    {
                        Console.CursorTop = y;
                        Console.CursorLeft = x;

                        switch (matrixInfo[y,x])
                        {
                            case 4:
                                Console.ForegroundColor = ConsoleColor.DarkGreen;
                                break;
                            case 3:
                                Console.ForegroundColor = ConsoleColor.Green;
                                break;
                            case 2:
                                Console.ForegroundColor = ConsoleColor.Gray;
                                break;
                            case 1:
                                Console.ForegroundColor = ConsoleColor.White;
                                break;
                            default: Console.ForegroundColor = ConsoleColor.Red;
                                break;
                        }
                        Console.Write(matrix.TheMatrix[y, x]);
                    }
                }
            }
            if(!_randomMode || _fullManualMode)
            {
                Console.CursorTop = matrix.TheMatrix.GetLength(0);
                Console.CursorLeft = _cursorPosition;
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write("^");
            }
            Console.ResetColor();
        }

        void fillMatrixInfo() 
        {
            for (int x = 0; x < matrixInfo.GetLength(0); x++)
            {
                for (int y = 0; y < matrixInfo.GetLength(1); y++)
                {
                    matrixInfo[x, y] = 0;
                }
            }
        }

        void UpdateCursor()
        {
            //Leer la tecla 
            var key = Console.ReadKey(true).Key;
            Console.ResetColor();

            switch (key)
            {
                case ConsoleKey.LeftArrow:
                    if (_cursorPosition == 0)
                        break;
                    _cursorPosition--;
                    break;
                case ConsoleKey.RightArrow:
                    if (_cursorPosition == matrix.TheMatrix.GetLength(1)-1)
                        break;
                    _cursorPosition++;
                    break;
                case ConsoleKey.D0:
                    _cursorPosition = 0;
                    break;
                case ConsoleKey.D1:
                    _cursorPosition = 1;
                    break;
                case ConsoleKey.D2:
                    _cursorPosition = 2;
                    break;
                case ConsoleKey.D3:
                    _cursorPosition = 3;
                    break;
                case ConsoleKey.D4:
                    _cursorPosition = 4;
                    break;
                case ConsoleKey.D5:
                    _cursorPosition = 5;
                    break;
                case ConsoleKey.D6:
                    _cursorPosition = 6;
                    break;
                case ConsoleKey.D7:
                    _cursorPosition = 7;
                    break;
                case ConsoleKey.D8:
                    _cursorPosition = 8;
                    break;
                case ConsoleKey.D9:
                    _cursorPosition = 9;
                    break;
            }
        }
    }
}
